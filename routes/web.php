<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('books','BookController@index');
Route::get('download','BookController@download');
Route::get('{id}/detail','BookController@detail');
Route::get('upload','BookController@create');
Route::post('upload','BookController@upload');

Route::get('administrator/panel','AdminstrationController@index');
Route::get('administrator/books','AdminstrationController@books');
Route::delete('administrator/books/{id}','AdminstrationController@destroy');
Route::delete('administrator/panel/{id}','AdminstrationController@userDelete');
Route::get('administrator/user/edit/{id}','AdminstrationController@edituser');
Route::post('administrator/user/edit/{id}','AdminstrationController@updateUser');
Route::get('administrator/books/download','AdminstrationController@downloadBook');
Route::post('administrator/books/upload','AdminstrationController@uploadBook');
Route::get('administrator/books/upload','AdminstrationController@createBook');
Route::get('administrator/books/{id}/detail','AdminstrationController@detailBook');
Route::get('administrator/books/edit/{id}','AdminstrationController@editBook');
Route::post('administrator/books/edit/{id}','AdminstrationController@updateBook');






