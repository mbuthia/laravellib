<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Book;
use App\Upload;
use Auth;
use Illuminate\Http\Request;

class BookController extends Controller {

    public function __construct() {
         $this->middleware('CheckAuth');
    }

    public function index() {
        $books = Book::all();
        return view('books.book', ['books' => $books]);
    }

    public function download() {
        $books = Book::all();
        return view('books.download', ['books' => $books]);
    }

    public function create() {
        return view('books.upload');
    }

    public function upload(Request $request) {

        $book = new Book;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->year = $request->year;
        $book->unit = $request->unit;
        $book->semester = $request->semester;
        if (Input::file('documents')->isValid()) {
            $destinationPath = 'documents'; //upload path
            $extension = Input::file('documents')->getClientOriginalExtension(); //get the upload extension
            $fileName = rand(11111, 99999) . '.' . $extension; //rename using random numbers
            Input::file('documents')->move($destinationPath, $fileName);  //upload
            $book->document = $fileName;
        }

        $book->user_id = Auth::user()->id;
        $book->save();

        $upload = new Upload;
        $upload->user_id = Auth::user()->id;
        $upload->book_id = $book->id;
        $upload->save();
        return redirect('books');
    }

    public function detail($id) {
        $book = Book::find($id);
        return view('books.detail', ['book' => $book]);
    }

}
