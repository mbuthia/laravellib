<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Book;
use App\Upload;
use App\User;
use Auth;
use Illuminate\Http\Request;
/**
 * Description of AdminstrationController
 *
 * @author dan
 */
class AdminstrationController {
    
    public function index(){
        $users = User::all();
        return view('admin.users',['users'=>$users]);
    }
    public function books(){
        $books = Book::all();
        
        return view('admin.book',['books' =>$books]);
    }
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return back();
    }
    public function userDelete($id){
        $user = User::find($id);
        $user->delete();
        return back();
    }
    public function editUser($id){
        $user = User::find($id);
        return view('admin.edituser',['user'=>$user]);
    }
    public function updateUser(request $request,$id){
        
        $user= User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->reg=$request->reg;
        $user->mobile=$request->mobile;
        $u= Input::get('admin');
        
        echo $user;
       
        
    }
     public function downloadBook() {
        $books = Book::all();
        return view('admin.download', ['books' => $books]);
    }
    public function createBook(){
        return view('admin.upload');
    }
    

    public function uploadBook(Request $request) {

        $book = new Book;
        $book->title = $request->title;
        $book->description = $request->description;
        $book->year = $request->year;
        $book->unit = $request->unit;
        $book->semester = $request->semester;
        if (Input::file('documents')->isValid()) {
            $destinationPath = 'documents'; //upload path
            $extension = Input::file('documents')->getClientOriginalExtension(); //get the upload extension
            $fileName = rand(11111, 99999) . '.' . $extension; //rename using random numbers
            Input::file('documents')->move($destinationPath, $fileName);  //upload
            $book->document = $fileName;
        }

        $book->user_id = Auth::user()->id;
        $book->save();

        $upload = new Upload;
        $upload->user_id = Auth::user()->id;
        $upload->book_id = $book->id;
        $upload->save();
        return redirect('books');
    }
    public function detailBook($id) {
        $book = Book::find($id);
        return view('admin.bookdetail', ['book' => $book]);
    }
    public function editBook($id){
        $book = Book::find($id);
        return view('admin.editbook',['book'=>$book]);
    }
    public function updateBook(request $request,$id){
        $book = Book::find($id);
        $book->title = $request->title;
        $book->description = $request->description;
        $book->unit = $request->unit;
        $book->year = $request->year;
        $book->semester = $request->semester;
        $book->save();
        return redirect('administrator/books');
    }
   
    
    

}



