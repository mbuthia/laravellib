<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
/**
 * Description of CheckAuth
 *
 * @author dan
 */
class CheckAuth {
    //put your code here
    public function handle($request, Closure $next, $guard = null)
    {
         if(auth()->user()->is_user==0){
            return redirect()->to('administrator/books');
        }

        return $next($request);
    }
}
