@extends('admin.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">  Users  </div>
                      
                <div class="panel-body">



                    <div class="col-sm-10  col-md-12">

                        <section>

                            <table id="example1" class="table table-advance table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th> Number</th>
                                        <th> Name</th>
                                        <th> Email</th>
                                        <th> Registration</th>
                                        <th>Mobile</th>
                                        <th>Admin</th>
                                        <th> Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1 ?>
                                    @foreach($users as $user )
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->reg}}</td>
                                        <td>{{$user->mobile}}</td>

                                        <td>
                                            <?php
                                            if ($user->is_user == 0) {
                                                echo 'Admin';
                                            } else {
                                                echo 'User';
                                            }
                                            ?>
                                        </td>

                                        <td class="col-md-2">
                                            <form action="/administrator/panel/{{$user->id}}" method="POST">
                                                {{ method_field('DELETE')}}
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="_token" value="{{ csrf_token()}} ">
                                                <a class="btn btn-primary btn-sm" href="/administrator/user/edit/{{$user->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')" name="name" value="Delete">
                                            </form>

                                        </td>
                                    </tr>

                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th> Number</th>
                                        <th> Name</th>
                                        <th> Email</th>
                                        <th> Registration</th>
                                        <th>Mobile</th>
                                        <th>Admin</th>
                                        <th> Action</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop




