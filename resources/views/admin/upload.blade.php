@extends('admin.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Upload</div>

                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data" action="/administrator/books/upload">
                        {{ csrf_field() }}
                        <div class="form-group {{ ($errors->has('title'))? $errors->first('title'):''}}">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter the title of the book">
                        </div>
                        <div class="form-group">
                            <label for="descrition">Description</label>
                            <input type="textarea" class="form-control" id="description" name="description" placeholder="Describe the book">
                        </div>
                        <div class="form-group">
                            <label for="year">Year</label>
                            <input type="text" class="form-control" id="year" name="year" placeholder="Approprite students of which year">
                        </div>
                        <div class="form-group">
                            <label for="unit">Unit</label>
                            <input type="text" class="form-control" id="unit" name="unit" placeholder="Book to which unit">
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <input type="text" class="form-control" id="semester" name="semester" placeholder="Book to which semester">
                        </div>

                        <div class="form-group">
                            <label for="document">Document</label>
                            <input type="file" id="file" name="documents">
                        </div>
                        <button class="btn btn-success">UPLOAD</button>
                    </form>

                    <div class="col-sm-9  col-md-10">

                        <section></section>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop


