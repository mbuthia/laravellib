@extends('admin.dashboard')
@section('content')
<div class="panel-body">

    <div class="form">
       
        <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="/administrator/books/edit/{{$book->id}}">
         {{ csrf_field() }}
            <div class="form-group ">
                <label for="Tite" class="control-label col-lg-2">Title</label>
                <div class="col-lg-10">
                    <input class="form-control" value="{{$book->title}}" id="title" name="title" minlength="5" type="text" required />
                </div>
            </div>
            <div class="form-group ">
                <label for="Descrption" class="control-label col-lg-2">Description</label>
                <div class="col-lg-10">
                    <input class="form-control " id="description" value="{{$book->description}}" type="text" name="description" required />
                </div>
            </div>
            <div class="form-group ">
                <label for="unit" class="control-label col-lg-2">Unit</label>
                <div class="col-lg-10">
                    <input class="form-control " id="unit" value="{{$book->unit}}" type="text" name="unit" />
                </div>
            </div>
            <div class="form-group ">
                <label for="Year" class="control-label col-lg-2">Year </label>
                <div class="col-lg-10">
                    <input class="form-control" id="year" value="{{$book->year}}" name="year" minlength="5" type="text" required />
                </div>
            </div> 
            <div class="form-group ">
                <label for="semester" class="control-label col-lg-2">Semester </label>
                <div class="col-lg-10">
                    <input class="form-control" id="semester" value="{{$book->semester}}" name="semester" minlength="5" type="text" required />
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-primary" type="submit">Save</button>
                    <button class="btn btn-default" type="button"><a href="/administrator/books"></a>Cancel</button>
                </div>
            </div>
        </form>

    </div>

</div>

@stop
