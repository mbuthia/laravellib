@extends('admin.dashboard')
@section('content')
<div class="panel-body">

    <div class="form">
       
        <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="/administrator/user/edit/{{$user->id}}">
         {{ csrf_field() }}
            <div class="form-group ">
                <label for="name" class="control-label col-lg-2">Name</label>
                <div class="col-lg-10">
                    <input class="form-control" value="{{$user->name}}" id="name" name="name" minlength="5" type="text" required />
                </div>
            </div>
            <div class="form-group ">
                <label for="email" class="control-label col-lg-2">E-Mail</label>
                <div class="col-lg-10">
                    <input class="form-control " id="cemail" value="{{$user->email}}" type="email" name="email" required />
                </div>
            </div>
            <div class="form-group ">
                <label for="curl" class="control-label col-lg-2">Registration</label>
                <div class="col-lg-10">
                    <input class="form-control " id="curl" value="{{$user->reg}}" type="text" name="reg" />
                </div>
            </div>
            <div class="form-group ">
                <label for="mobile" class="control-label col-lg-2">Mobile </label>
                <div class="col-lg-10">
                    <input class="form-control" id="subject" value="{{$user->mobile}}" name="mobile" minlength="5" type="text" required />
                </div>
            </div>                                      
            <div class="form-group " >
                <select name="dropdown" name="admin" id="admin" class="col-lg-offset-2">
                    <option value="1" >User</option>'
                    <option value="0" >Admin</option>    
                </select>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-primary" type="submit">Save</button>
                    <button class="btn btn-default" type="button">Cancel</button>
                </div>
            </div>
        </form>

    </div>

</div>

@stop
