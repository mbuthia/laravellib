@extends('admin.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Books available</div>

                <div class="panel-body">



                    <div class="col-sm-10  col-md-12">

                        <section>

                            <table id="example1" class="table table-advance table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Semester</th>

                                        <th>Download</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1 ?>
                                    @foreach($books as $book )
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$book->title}}</td>
                                        <td>{{$book->description}}</td>
                                        <td>{{$book->unit}}</td>
                                        <td>{{$book->semester}}</td>
                                        <td>
                                            <a href="{{url('documents/'.$book->document)}}">Download</a>
                                        </td>
                                    </tr>

                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Number</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Semester</th>
                                        <th>Download</th>
                                       
                                    </tr>
                                </tfoot>
                            </table>

                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop




