@extends('admin.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Books available</div>

                <div class="panel-body">



                    <div class="col-sm-10  col-md-12">

                        <section>

                            <table id="example1" class="table table-advance table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Number</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Semester</th>

                                        <th>View Details</th>
                                        <th class="col-md-offset-2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1 ?>
                                    @foreach($books as $book )
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$book->title}}</td>
                                        <td>{{$book->description}}</td>
                                        <td>{{$book->unit}}</td>
                                        <td>{{$book->semester}}</td>
                                        <td><a href="/administrator/books/{{$book->id}}/detail" >Details</a></td>
                                        <td class="col-md-2">
                                            <form action="/administrator/books/{{$book->id}}" method="POST">
                                                {{ method_field('DELETE')}}
                                                <input type="hidden" name="_method" value="delete">
                                                <input type="hidden" name="_token" value="{{ csrf_token()}} ">
                                                 <a class="btn btn-primary btn-sm" href="/administrator/books/edit/{{$book->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')" name="name" value="Delete">
                                            </form>
           
                                        </td>
                                    </tr>

                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Number</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Semester</th>
                                        <th>View Details</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop




