<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">

        <title>Somo Admin</title>


        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/bootstrap-theme.css') }}" rel="stylesheet">

        <link href="{{ asset('css/elegant-icons-style.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" />    

        <link href="{{ asset('assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/fullcalendar/fullcalendar/fullcalendar.css') }}" rel="stylesheet" />

        <link href="{{ asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen"/>

        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" type="text/css">
        <link href="{{ asset('css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}">
        <link href="{{ asset('css/widgets.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/xcharts.min.css') }}" rel=" stylesheet">	
        <link href="{{ asset('css/jquery-ui-1.10.4.min.css') }}" rel="stylesheet">

    </head>

    <body>
        <!-- container section start -->
        <section id="container" class="">


            <header class="header dark-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
                </div>

                <!--logo start-->
                <a href="panel" class="logo">Somo <span class="lite">Admin</span></a>
                <!--logo end-->

                <div class="nav search-row" id="top_menu">
                    <!--  search form start -->
                    <ul class="nav top-menu">                    
                        <li>
                            <form class="navbar-form">
                                <input class="form-control" placeholder="Search" type="text">
                            </form>
                        </li>                    
                    </ul>
                    <!--  search form end -->                
                </div>

                <div class="top-nav notification-row">                

                    <ul class="nav pull-right top-menu">

                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="{{asset('img/avatar1_small.jpg')}}">
                                </span>
                                <span class="username"> {{ Auth::user()->name }}</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>

                            </ul>
                        </li>

                    </ul>

                </div>
            </header>      

            <aside>
                <div id="sidebar"  class="nav-collapse ">

                    <ul class="sidebar-menu">                
                        <li class="active">
                            <a class="" href="/administrator/books">
                                <i class="icon_house_alt"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                         <li>
                      <a class="" href="/administrator/panel">
                          <i class="fa fa-user"></i>
                          <span>Users</span>
                      </a>
                  </li>
                      <li>
                      <a class="" href="/administrator/books">
                          <i class="fa fa-book"></i>
                          <span>Books</span>
                      </a>
                  </li>
                      <li>
                      <a class="" href="/administrator/books/download">
                          <i class="fa fa-download"></i>
                          <span>Downloads</span>
                      </a>
                  </li>
                      <li>
                      <a class="" href="/administrator/books/upload">
                          <i class="fa fa-upload"></i>
                          <span>Upload</span>
                      </a>
                  </li>
                     


                    </ul>

                </div>
            </aside>



            <section id="main-content">
                <section class="wrapper">            
                    <!--overview start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
                                 @yield('content')
                        </div>
                    </div>
                </section>
            </section>
        </section>



        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui-1.10.4.min.js') }}"></script>
        <script src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-ui-1.9.2.custom.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/jquery-knob/js/jquery.knob.js') }}"></script>
        <script src="{{ asset('js/jquery.sparkline.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.js') }}" ></script>
        <script src="{{ asset('js/fullcalendar.min.js') }}"></script> 
        <script src="{{ asset('assets/fullcalendar/fullcalendar/fullcalendar.js') }}"></script>
        <script src="{{ asset('js/calendar-custom.js') }}"></script>
        <script src="{{ asset('js/jquery.rateit.min.js') }}"></script>
        <script src="{{ asset('js/jquery.customSelect.min.js') }}" ></script>
        <script src="{{ asset('assets/chart-master/Chart.js') }}"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="{{ asset('js/sparkline-chart.js') }}"></script>
        <script src="{{ asset('js/easy-pie-chart.js') }}"></script>
        <script src="{{ asset('js/jquery-jvectormap-1.2.2.min.js') }}"></script>
        <script src="{{ asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('js/xcharts.min.js') }}"></script>
        <script src="{{ asset('js/jquery.autosize.min.js') }}"></script>
        <script src="{{ asset('js/jquery.placeholder.min.js') }}"></script>
        <script src="{{ asset('js/gdp-data.js') }}"></script>	
        <script src="{{ asset('js/morris.min.js') }}"></script>
        <script src="{{ asset('js/sparklines.js') }}"></script>	
        <script src="{{ asset('js/charts.js') }}"></script>
        <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>


    </body>
</html>