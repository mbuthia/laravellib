@extends('admin.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Details</div>

                <div class="panel-body">
                    Id:         {{$book->id}}<br>
                    Title:      {{$book->title}}<br>
                    Description: {{$book->description}}<br>
                    Unit:       {{$book->unit}}<br>
                    Semester:    {{$book->semester}}<br>
                    Download:    <a href="{{url('documents/'.$book->document)}}" >Download</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


