-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2017 at 10:15 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dan`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `description`, `year`, `semester`, `unit`, `document`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'laravel', 'php framework', '2017-06-06', 'sem2', 'computer programming', '', 1, '2017-06-05 21:00:00', '2017-06-29 21:00:00'),
(3, 'python', 'python', 'year4', 'sem2', 'computer programming', '69616.docx', 2, '2017-07-13 16:31:46', '2017-07-13 16:31:46'),
(4, 'system programming', 'introduction to system programming', 'year4', 'sem2', 'computer programming', '40364.doc', 2, '2017-09-08 06:23:33', '2017-09-08 06:23:33'),
(5, 'computer programming II', 'python coding', 'year4', 'sem2', 'computer programming', '43592.odt', 2, '2017-09-08 06:29:19', '2017-09-08 06:29:19'),
(6, 'sassssssssssssssssssss', 'dsss', 'year4', 'sem2', 'computer programming', '97160.txt', 2, '2017-09-09 03:19:02', '2017-09-12 13:27:28'),
(7, 'dddd', 's', 'r', 'sem2', 'x', '73734.pdf', 2, '2017-09-09 03:44:37', '2017-09-09 03:44:37'),
(8, 'dan', 'mbuthiaaaaaaaa', 'yr 3', 's2', 'computer programming', '15936.png', 2, '2017-09-12 11:38:48', '2017-09-12 11:38:48');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_06_17_180101_create_books_table', 1),
(4, '2017_06_17_180236_create_courses_table', 1),
(5, '2017_06_17_180302_create_downloads_table', 1),
(6, '2017_06_17_180328_create_uploads_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `user_id`, `book_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2017-07-14 07:24:40', '2017-07-14 07:24:40'),
(2, 2, 1, '2017-09-08 06:23:33', '2017-09-08 06:23:33'),
(3, 2, 1, '2017-09-08 06:29:19', '2017-09-08 06:29:19'),
(4, 2, 1, '2017-09-09 03:19:03', '2017-09-09 03:19:03'),
(5, 2, 7, '2017-09-09 03:44:37', '2017-09-09 03:44:37'),
(6, 2, 8, '2017-09-12 11:38:49', '2017-09-12 11:38:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_user` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `reg`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `is_user`) VALUES
(1, 'Daniel', 'dambuthia@outlook.com', '0705798062', 'tu01-sc211-0127/2013', '0', '$2y$10$NSp4zA4VMqeystFPREF0Y.WIGMIeUJ63KMKHwo3XWECnci8hi/GUa', '8QKutbz9fxFMKGBbkmiJVc3qdr00yMTGQewqair7Z2sm4aKylZ76oY2hK1yp', '2017-06-18 02:35:49', '2017-06-18 02:35:49', 0),
(2, 'Mburu', 'mburu@mburu.com', '0719551826', 'tu01-sc211-0128/2013', '0', '$2y$10$P2jHNHHe8NOF2eias.hzEu7SkGY6SskqZyMYlWOhhJeJBaErR6Epa', 'tsNOgf7L6T4YzNiuNWtK5plveVWOINt1WoaFycf0yUdsNLw5j2DFc1tJucgc', '2017-06-18 02:37:40', '2017-06-18 02:37:40', 0),
(3, 'Thiru', 'thiru@thiru.com', '0714876995', 'tu01-sc211-0134/2013', '0', '$2y$10$yijYuH9AhAZdRTT8F9fFJ.4czbwYxEMzUoa.NNrj50YOQMZPAtvY.', '6hYVyFiiHXV8voJmTmALN5Qo3MBoLP17gwrUI8q4MgEu8LdmNXUsujEjLEED', '2017-06-18 02:39:20', '2017-06-18 02:39:20', 0),
(4, 'dan', 'daggi@daggi.com', '0724808805', 'tu01-sc211-0137/2013', '0', '$2y$10$dHNA3k7g7Ddu4MwZ4NCS3es0qtDqc8k/Nacrjv3SLrw8X6g9AZnGq', 'yKwOgJdhCEJbTaKm7qQgLHuk3OWdzUFpYV62ZRq9L4Ep45jIeLLgmLic8a5i', '2017-07-12 08:45:01', '2017-07-12 08:45:01', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_user_id_index` (`user_id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `downloads_user_id_index` (`user_id`),
  ADD KEY `downloads_book_id_index` (`book_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_index` (`user_id`),
  ADD KEY `uploads_book_id_index` (`book_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `downloads`
--
ALTER TABLE `downloads`
  ADD CONSTRAINT `downloads_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `downloads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
